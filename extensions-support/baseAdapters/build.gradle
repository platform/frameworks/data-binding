/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
apply plugin: 'com.android.library'

android {
    compileSdkVersion dataBindingConfig.compileSdkVersion
    buildToolsVersion dataBindingConfig.buildToolsVersion
    buildFeatures {
        dataBinding = true
    }
    dataBinding {
        addDefaultAdapters = false
    }
    defaultConfig {
        minSdkVersion 14
        targetSdkVersion dataBindingConfig.targetSdkVersion
        versionCode 1
        versionName "1.0"
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }
    compileOptions {
        sourceCompatibility dataBindingConfig.javaTargetCompatibility
        targetCompatibility dataBindingConfig.javaSourceCompatibility
    }
    packagingOptions {
        exclude 'META-INF/services/javax.annotation.processing.Processor'
        exclude 'META-INF/LICENSE.txt'
        exclude 'META-INF/NOTICE.txt'
    }
}

dependencies {
    // we don't care about version of these because they are not listed as dependencies in the
    // final maven artifact.
    implementation project(":library")
    compileOnly rootProject.ext.libs.android_support_core_utils
    compileOnly rootProject.ext.libs.android_support_cardview_v7
    compileOnly rootProject.ext.libs.android_support_appcompat_v7
}

//create jar tasks
android.libraryVariants.all { variant ->
    def name = variant.buildType.name

    if (name.equals(com.android.builder.core.BuilderConstants.DEBUG)) {
        return; // Skip debug builds.
    }
    def suffix = name.capitalize()

    def sourcesJarTask = project.tasks.create(name: "sourceJar${suffix}", type: Jar) {
        classifier = 'sources'
        from android.sourceSets.main.java.srcDirs
    }
}

afterEvaluate {
    publishing {
        publications {
            release(MavenPublication) {
                artifactId = 'adapters'
                from components.release
                artifact project.tasks.named("sourceJarRelease")
                pom {
                    licenses {
                        license {
                            name = dataBindingConfig.licenseName
                            url = dataBindingConfig.licenseUrl
                            distribution = dataBindingConfig.licenseDistribution
                        }
                    }
                }
            }
        }
    }
}

task prebuild(type : Copy) {
    dependsOn tasks.named("publish")
    from "$buildDir/outputs/aar/baseAdapters-release.aar"
    into dataBindingConfig.prebuildFolder
    rename { String fileName ->
        "databinding-adapters.aar"
    }
}
